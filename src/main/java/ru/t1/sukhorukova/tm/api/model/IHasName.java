package ru.t1.sukhorukova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
