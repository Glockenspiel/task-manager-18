package ru.t1.sukhorukova.tm;

import ru.t1.sukhorukova.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
