package ru.t1.sukhorukova.tm.enumerated;

import ru.t1.sukhorukova.tm.comparator.CreatedComparator;
import ru.t1.sukhorukova.tm.comparator.NameComporator;
import ru.t1.sukhorukova.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComporator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;
    private final Comparator comprator;

    Sort(String displayName, Comparator comprator) {
        this.displayName = displayName;
        this.comprator = comprator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort: values()) {
            if(sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComprator() {
        return comprator;
    }

}
