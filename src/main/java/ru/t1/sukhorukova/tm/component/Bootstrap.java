package ru.t1.sukhorukova.tm.component;

import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.command.project.*;
import ru.t1.sukhorukova.tm.command.system.*;
import ru.t1.sukhorukova.tm.command.task.*;
import ru.t1.sukhorukova.tm.command.user.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sukhorukova.tm.exception.system.CommandNotSupportedException;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class Bootstrap  implements ILocatorService {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationHelpCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new SystemInfoCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());

        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());

        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());

        registry(new TaskListByProjectIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new ApplicationExitCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setLocatorService(this);
        commandService.add(command);
    }

    public void start(final String[] args) {
        initLogger();
        processArguments(args);
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (final RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() {
        projectService.add(new Project("PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project("PROJECT_18", "Test project 18", Status.IN_PROGRESS));
        projectService.add(new Project("PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project("PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task("TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task("TASK_18", "Test task 18", Status.IN_PROGRESS));
        taskService.add(new Task("TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task("TASK_26", "Test task 26", Status.IN_PROGRESS));

        userService.create("USER_01", "user01", "user01@address.ru");
        userService.create("USER_02", "user02", Role.ADMIN);
        userService.create("USER_03", "user03", "user03@address.ru");
    }

}
