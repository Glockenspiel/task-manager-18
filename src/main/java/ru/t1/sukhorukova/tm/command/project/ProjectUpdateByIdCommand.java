package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-id";
    public static final String DESCRIPTION = "Update project by id.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        getProjectService().updateById(id, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
