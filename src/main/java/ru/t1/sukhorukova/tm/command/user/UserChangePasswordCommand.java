package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private final String NAME = "user-change-password";
    private final String DESCRIPTION = "Change password.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");

        System.out.println("Enter new password:");
        final String password = TerminalUtil.nextLine();

        final String userId = getAuthService().getUserId();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
