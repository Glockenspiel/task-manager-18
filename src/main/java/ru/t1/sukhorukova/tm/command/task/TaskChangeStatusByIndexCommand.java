package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-change-status-by-index";
    public static final String DESCRIPTION = "Change task status by index.";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(index, status);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
