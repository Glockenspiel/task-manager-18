package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-index";
    public static final String DESCRIPTION = "Complete project by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        getProjectService().changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
