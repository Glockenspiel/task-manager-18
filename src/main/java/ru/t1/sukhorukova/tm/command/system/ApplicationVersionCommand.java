package ru.t1.sukhorukova.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";
    public static final String NAME = "version";
    public static final String DESCRIPTION = "Show version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
