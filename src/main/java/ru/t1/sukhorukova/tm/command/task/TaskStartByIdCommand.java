package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-id";
    public static final String DESCRIPTION = "Start task by id.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        getTaskService().changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
