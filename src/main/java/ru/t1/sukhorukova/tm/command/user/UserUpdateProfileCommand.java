package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-update-profile";
    private final String DESCRIPTION = "Update profile user.";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        final String userId = getAuthService().getUserId();

        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();

        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();

        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();

        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
