package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-index";
    public static final String DESCRIPTION = "Update task by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description:");
        final String description = TerminalUtil.nextLine();

        getTaskService().updateByIndex(index, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
