package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-id";
    public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
