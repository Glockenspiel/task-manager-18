package ru.t1.sukhorukova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";
    public static final String NAME = "about";
    public static final String DESCRIPTION = "Show about developer.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Victoria Sukhorukova");
        System.out.println("vsukhorukova@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
