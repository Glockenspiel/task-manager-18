package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-id";
    public static final String DESCRIPTION = "Change project status by id.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusById(id, status);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
