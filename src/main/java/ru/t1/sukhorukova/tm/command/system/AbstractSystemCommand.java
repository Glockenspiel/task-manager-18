package ru.t1.sukhorukova.tm.command.system;

import ru.t1.sukhorukova.tm.api.service.ICommandService;
import ru.t1.sukhorukova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getLocatorService().getCommandService();
    }

}
