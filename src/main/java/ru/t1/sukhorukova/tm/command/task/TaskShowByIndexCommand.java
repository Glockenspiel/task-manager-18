package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-index";
    public static final String DESCRIPTION = "Display task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");

        System.out.println("Enter task index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
