package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";
    public static final String DESCRIPTION = "Show project list.";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        int index = 1;
        final List<Project> projects = getProjectService().findAll(sort);
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index++ + ". " + project.getName() + ": " + project.getDescription());
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
