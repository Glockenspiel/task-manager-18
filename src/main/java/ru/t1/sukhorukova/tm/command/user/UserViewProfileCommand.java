package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-view-profile";
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        final User user = getAuthService().getUser();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Role: " + user.getRole());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
