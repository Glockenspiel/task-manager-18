package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";
    public static final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description");
        final String description = TerminalUtil.nextLine();

        getTaskService().create(name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
