package ru.t1.sukhorukova.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "user-logout";
    private final String DESCRIPTION = "Log out.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
