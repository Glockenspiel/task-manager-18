package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-id";
    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
