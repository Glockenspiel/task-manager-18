package ru.t1.sukhorukova.tm.command.user;

import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private final String NAME = "user-login";
    private final String DESCRIPTION = "Sign in.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");

        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();

        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();

        getAuthService().login(login, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
