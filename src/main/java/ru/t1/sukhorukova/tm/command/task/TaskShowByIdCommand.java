package ru.t1.sukhorukova.tm.command.task;

import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";
    public static final String DESCRIPTION = "Display task by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");

        System.out.println("Enter task id:");
        final String id = TerminalUtil.nextLine();

        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
