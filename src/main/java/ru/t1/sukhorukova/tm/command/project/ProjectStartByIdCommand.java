package ru.t1.sukhorukova.tm.command.project;

import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-start-by-id";
    public static final String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
