package ru.t1.sukhorukova.tm.command;

import ru.t1.sukhorukova.tm.api.command.ICommand;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;

public abstract class AbstractCommand implements ICommand {

    protected ILocatorService locatorService;

    public abstract void execute();

    public abstract String getArgument();

    public abstract String getName();

    public abstract String getDescription();

    public ILocatorService getLocatorService() {
        return locatorService;
    }

    public void setLocatorService(final ILocatorService locatorService) {
        this.locatorService = locatorService;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;

        return displayName;
    }

}
