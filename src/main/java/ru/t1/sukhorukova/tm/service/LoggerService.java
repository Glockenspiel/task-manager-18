package ru.t1.sukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private final String configFile = "/logger.properties";
    private final String commands = "COMMANDS";
    private final String commandsFile = "./commands.xml";
    private final String errors = "ERRORS";
    private final String errorsFile = "./errors.xml";
    private final String messages = "MESSAGES";
    private final String messagesFile = "./messages.xml";
    private final LogManager manager = LogManager.getLogManager();
    private final Logger loggerRoot = Logger.getLogger("");
    private final Logger loggerCommand = Logger.getLogger(commands);
    private final Logger loggerError = Logger.getLogger(errors);
    private final Logger loggerMessage = Logger.getLogger(messages);
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    public Logger getLoggerCommand() {
        return loggerCommand;
    }

    public Logger getLoggerError() {
        return loggerError;
    }

    public Logger getLoggerMessage() {
        return loggerMessage;
    }

    private @NotNull ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public LoggerService() {
        loadConfigFromFile();
        registry(loggerCommand, commandsFile, false);
        registry(loggerError, errorsFile, true);
        registry(loggerMessage, messagesFile, true);
    }

    private void loadConfigFromFile() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(configFile));
        } catch (final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    private void registry(final Logger logger,
                          final String fileName,
                          final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    @Override
    public void info (final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        loggerCommand.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;;
        loggerError.log(Level.SEVERE, e.getMessage(), e);
    }

}
