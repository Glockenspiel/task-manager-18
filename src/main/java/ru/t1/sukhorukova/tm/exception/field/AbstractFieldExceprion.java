package ru.t1.sukhorukova.tm.exception.field;

import ru.t1.sukhorukova.tm.exception.AbstractException;

public abstract class AbstractFieldExceprion extends AbstractException {

    public AbstractFieldExceprion() {
    }

    public AbstractFieldExceprion(String message) {
        super(message);
    }

    public AbstractFieldExceprion(Throwable cause) {
        super(cause);
    }

    public AbstractFieldExceprion(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldExceprion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
